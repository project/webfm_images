/* namespace */
function webfmImages() {}

if (Drupal.jsEnabled) {
  $(window).load(webfmImagesGetMenusAjax);
}

webfmImages.ajaxUrl = function() {
  var path = getBaseUrl() + "/?q=";
  return path += (typeof getModUrl == "undefined") ? "webfm_images_js" : getModUrl();
}

function webfmImagesGetMenusAjax() {
  var url = webfmImages.ajaxUrl();
  Webfm.progressObj.show(Webfm.js_msg["work"],  "blue");
  var postObj = {action:encodeURIComponent("get_menus")};
  Webfm.HTTPPost(url, webfmImages.GetMenusCallback, '', postObj);
}

webfmImages.GetMenusCallback = function(string, xmlhttp, obj) {
  Webfm.progressObj.hide();
  Webfm.alrtObj.msg();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
//    Webfm.dbgObj.dbg("GetMenusCallback:", Webfm.dump(result));
    if (result.status) {
      var menus = [];
      menus = result.data;
      for(var i = 0; i < menus.length; i++) {
        Webfm.menuHT.put('file', new Webfm.menuElement("Resize to "+menus[i], webfmImages.menuResize, webfmImages.menuTest));
      }
    } else {
      Webfm.alrtObj.msg(result.data);
    }
  } else {
    Webfm.alrtObj.msg(Webfm.js_msg["ajax-err"]);
  }
}

webfmImages.menuResize = function(obj){
  var url = webfmImages.ajaxUrl();
  var path = obj.element.title;
  Webfm.progressObj.show(Webfm.js_msg["work"],  "blue");
  var postObj = {action:encodeURIComponent(this.desc), filepath:encodeURIComponent(path)};
  Webfm.dbgObj.dbg("postObj:", Webfm.dump(postObj));
  Webfm.HTTPPost(url, webfmImages.ResizeCallback, obj, postObj);
}

webfmImages.ResizeCallback = function(string, xmlhttp, obj) {
  Webfm.progressObj.hide();
  Webfm.alrtObj.msg();
  if (xmlhttp.status == 200) {
    var result = Drupal.parseJson(string);
//    Webfm.dbgObj.dbg("resize result:", Webfm.dump(result));
    if (result.status) {
      Webfm.dirListObj.refresh();
//      Webfm.alrtObj.str_arr(result.data);
    } else {
      Webfm.alrtObj.msg(result.data);
    }
  } else {
    Webfm.alrtObj.msg(Webfm.js_msg["ajax-err"]);
  }
}

webfmImages.menuTest = function(obj) {
  switch(obj.ext.toLowerCase()) {
    case 'jpg':
    case 'jpeg':
    case 'png':
    case 'bmp':
    case 'gif':
      return true;
    default:
      return false;
  }
}
